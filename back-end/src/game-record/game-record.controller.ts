import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Req,
  UseGuards,
} from '@nestjs/common';
import { GameRecordService } from './game-record.service';
import { CreateGameRecorDTO } from './dtos/create-game-record.dto';
import { GameEntity } from '../game/game.entity';
import { GameByIdPipe } from '../game/pipes/game-by-id.pipe';
import { AverageRecordDto } from './dtos/average-record.dto';
import { LastRecordDto } from './dtos/last-record.dto';
import { GameRecordEntity } from './game-record.entity';
import { GameRecordByIdPipe } from './pipe/game-record-by-id.pipe';
import { RecordsPerTryDTO } from './dtos/records-per-try.dto';
import { AverageRecordsPerGameDTO } from './dtos/average-records-per-game.dto';
import { DetailsPerRecordDTO } from './dtos/details-per-record.dto';
import { DetailRecordService } from '../detail-record/detail-record.service';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserByIdPipe } from '../user/pipes/user-by-id.pipe';
import { UserEntity } from '../user/user.entity';
import { AverageRecordsPerSubjectTO } from './dtos/average-records-per-subject.dto';
import { IsOwnerOrTutorOrAdminGuard } from '../guards/IsOwnerOrTutorOrAdmin.guard';

@Controller('game-record')
@UseGuards(JwtAuthGuard)
// @UseGuards(JwtAuthGuard) // TODO es admin o dueno
export class GameRecordController {
  constructor(
    private _gameRecordService: GameRecordService,
    private _detailRecordService: DetailRecordService,
  ) {}

  @Get(':gameId/average')
  findAverage(
    @Param() averageRecordDto: AverageRecordDto, // TODO mal nombrado el param
    @Param('gameId', GameByIdPipe) game: GameEntity,
  ) {
    return this._gameRecordService.findAverageScore(game.id);
  }

  @Get(':gameId/last')
  findLast(
    @Param() lastRecordDto: LastRecordDto,
    @Req() req,
    @Param('gameId', GameByIdPipe) game: GameEntity,
  ) {
    return this._gameRecordService.findLastUserScore(req.user.id, game.id);
  }

  @Post()
  create(
    @Body() createGameRecordDTO: CreateGameRecorDTO,
    @Req() req,
    @Body('game', GameByIdPipe) game: GameEntity,
  ) {
    return this._gameRecordService.createManyRecords(
      createGameRecordDTO,
      req.user.id,
    );
  }

  @Get(':userId/:gameId/recordsPerGame')
  @UseGuards(IsOwnerOrTutorOrAdminGuard)
  recordsPerTry(
    @Param() recordsPerTryDTO: RecordsPerTryDTO,
    @Param('userId', UserByIdPipe) user: UserEntity,
    @Param('gameId', GameByIdPipe) game: GameEntity,
  ) {
    return this._gameRecordService.findRecordsPerGame(user.id, game.id);
  }

  @Get(':userId/averageRecordsPerSubject')
  @UseGuards(IsOwnerOrTutorOrAdminGuard)
  averageRecordsPerSubject(
    @Param() averageRecordsPerSubjectDTO: AverageRecordsPerSubjectTO,
    @Param('userId', UserByIdPipe) user: UserEntity,
  ): Promise<Array<{ average: number; subject: string }>> {
    return this._gameRecordService.averageRecordsPerSubject(user.id);
  }

  @Get(':userId/:subjectName/averageRecordsPerGame')
  @UseGuards(IsOwnerOrTutorOrAdminGuard)
  averageRecordsPerGame(
    @Param() averageRecordsPerGameDTO: AverageRecordsPerGameDTO,
    @Param('userId', UserByIdPipe) user: UserEntity,
    @Param('subjectName') subject: string,
  ): Promise<Array<{ average: number; game: string }>> {
    return this._gameRecordService.averageRecordsPerGame(user.id, subject);
  }

  @Get(':gameRecordId/detailsPerRecord')
  detailsPerRecord(
    @Param() detailsPerRecordDTO: DetailsPerRecordDTO,
    @Param('gameRecordId', GameRecordByIdPipe) gameRecord: GameRecordEntity,
  ) {
    return this._detailRecordService.find(gameRecord.id.toString());
  }

  @Get('coins')
  coinsPerUSer(@Req() req) {
    return this._gameRecordService.coinsPerUser(req.user.id);
  }
}
