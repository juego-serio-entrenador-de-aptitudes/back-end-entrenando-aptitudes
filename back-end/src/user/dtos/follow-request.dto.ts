import { IsNumber, IsPositive } from 'class-validator';

export class FollowRequestDTO {
  @IsNumber()
  @IsPositive()
  playerId: number;
}
