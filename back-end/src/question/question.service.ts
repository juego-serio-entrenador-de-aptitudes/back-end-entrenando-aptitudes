import { Injectable, NotAcceptableException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { QuestionEntity } from './question.entity';
import { Repository, Like } from 'typeorm';
import { GameIdDefinitions } from '../definitions/game-id.definitions';
import { UpdateQuestionDto } from './dtos/update-question.dto';
import { SearchQuestionCriteriaInterface } from './interfaces/search-question-criteria.interface';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(QuestionEntity)
    private readonly _questionRepository: Repository<QuestionEntity>,
  ) {}

  async createMany(questions: any[]) {
    questions = questions.map(question => {
      const gameId = question.game;
      switch (gameId) {
        case GameIdDefinitions.M_REGLA_TRES:
          question.correctOptionText = JSON.stringify(
            question.correctOptionText,
          );
          break;
        case GameIdDefinitions.L_ANALOGIAS_MUSICALES:
          question.questionText = JSON.stringify(question.questionText);
          break;
        case GameIdDefinitions.L_TRIVIA_EN_FERIA:
          question.questionText = JSON.stringify(question.questionText);
          break;
        default:
          break;
      }
      return question;
    });
    return await this._questionRepository.save(questions);
  }

  createOne(question: QuestionEntity) {
    return this._questionRepository.save(question);
  }

  findOneOrFail(id: string) {
    return this._questionRepository.findOneOrFail(id);
  }

  async sendRandomQuestions(gameId: number, quantity: number) {
    const questionsQuantity = await this._questionRepository.count({
      where: { game: gameId },
    });

    const enoughQuestions = questionsQuantity >= quantity;
    if (enoughQuestions) {
      return { questions: await this.getRandomQuestions(quantity, gameId) };
    } else {
      throw new NotAcceptableException(
        'no hay la cantidad de preguntas solicitas',
      );
    }
  }

  async getRandomQuestions(quantity: number, gameId: number) {
    return await this._questionRepository
      .createQueryBuilder('question')
      .where('question.game = :gameId', { gameId })
      .orderBy('RAND()')
      .limit(quantity)
      .getMany();
  }

  createAgilidadNumericaQuestions() {
    const questions = [];
    for (let i = 2; i < 10; i++) {
      for (let j = 5; j < 10; j++) {
        questions.push(this.createMultiplicationQuestion(i, j));
        questions.push(this.createDivision(i * j, j));
        questions.push(this.createSum(i * j, j));
        questions.push(this.createSubstraction(i * j, j));
      }
    }
    return this.createMany(questions);
  }

  private createMultiplicationQuestion(number1: number, number2: number) {
    const questionText = `${number1} * ${number2}`;
    const correctOptionText = `${number1 * number2}`;
    return {
      questionText,
      correctOptionText,
      game: GameIdDefinitions.M_AGILIDAD_NUMERICA,
    };
  }

  private createDivision(number1: number, number2: number) {
    const questionText = `${number1} / ${number2}`;
    const correctOptionText = `${number1 / number2}`;
    return {
      questionText,
      correctOptionText,
      game: GameIdDefinitions.M_AGILIDAD_NUMERICA,
    };
  }

  private createSum(number1: number, number2: number) {
    const questionText = `${number1} + ${number2}`;
    const correctOptionText = `${number1 + number2}`;
    return {
      questionText,
      correctOptionText,
      game: GameIdDefinitions.M_AGILIDAD_NUMERICA,
    };
  }

  private createSubstraction(number1: number, number2: number) {
    const questionText = `${number1} - ${number2}`;
    const correctOptionText = `${number1 - number2}`;
    return {
      questionText,
      correctOptionText,
      game: GameIdDefinitions.M_AGILIDAD_NUMERICA,
    };
  }

  updateQuestion(options: UpdateQuestionDto): Promise<QuestionEntity> {
    const { id, ...updates } = options;
    return this._questionRepository
      .update(id, updates)
      .then(() => this._questionRepository.findOne(id));
  }

  searchQuestions({
    game,
    questionText = '',
    skip = 0,
    take = 10,
  }: SearchQuestionCriteriaInterface): Promise<QuestionEntity[]> {
    const where: any = [
      { questionText: Like(`%${questionText}%`) },
      { questionImg: Like(`%${questionText}%`) },
    ];
    if (game) {
      where[0].game = game;
      where[1].game = game;
    }
    return this._questionRepository.find({
      where,
      skip,
      take,
      loadRelationIds: true,
    });
  }

  countTotalQuestions({
    game,
    questionText = '',
  }: SearchQuestionCriteriaInterface): Promise<number> {
    const where: any = [
      { questionText: Like(`%${questionText}%`), game },
      { questionImg: Like(`%${questionText}%`), game },
    ];
    if (game) {
      where[0].game = game;
      where[1].game = game;
    }
    return this._questionRepository.count({
      where,
    });
  }

  deleteQuestion(id: number) {
    return this._questionRepository.delete({
      id,
    });
  }
}
