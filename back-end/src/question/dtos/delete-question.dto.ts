import { IsNumberString } from 'class-validator';

export class DeleteQuestionDTO {
  @IsNumberString()
  id: number;
}
