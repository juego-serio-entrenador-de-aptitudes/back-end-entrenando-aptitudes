
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ROL } from '../definitions/rol.definition';
import { UserService } from '../user/user.service';

@Injectable()
export class IsOwnerOrTutorOrAdminGuard implements CanActivate {

    constructor(
        private readonly _userService: UserService,
    ) {}

    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        const isAdmin = this.isAdmin(request.user);
        if ( isAdmin ) {
            return true;
        }

        const isOwner = this.isOwner(request.params.userId, request.user.id);
        if ( isOwner ) {
            return true;
        }

        return this.isTutor(request.params.userId, request.user.id);
    }

    isAdmin(user: {rol: ROL}): boolean {
        return user.rol === ROL.ADMIN;
    }

    isOwner(userId: number, requesterId: number): boolean {
        if (typeof userId === 'string') {
            userId = parseInt(userId, 10);
        }
        return userId === requesterId;
    }

    async isTutor(userId: number, requesterId: number): Promise<boolean> {
        const tutorsId = await this._userService.findTutorsId(userId);
        return tutorsId.some(tutorId => tutorId === requesterId);
    }
}
