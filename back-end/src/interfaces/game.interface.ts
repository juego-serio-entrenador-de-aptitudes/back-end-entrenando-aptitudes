import { QuestionInterface } from './question.interface';

export interface GameInterface {
  id?: number;
  subject: string;
  name: string;
  questions?: QuestionInterface[];
  records?;
}
