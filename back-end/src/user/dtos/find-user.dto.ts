import { IsNumberString } from 'class-validator';

export class FindUserDTO {
  @IsNumberString()
  id: string;
}
