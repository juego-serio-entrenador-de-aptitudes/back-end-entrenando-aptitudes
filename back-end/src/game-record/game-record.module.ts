import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GameRecordEntity } from './game-record.entity';
import { GameRecordService } from './game-record.service';
import { GameRecordController } from './game-record.controller';
import { UserModule } from 'src/user/user.module';
import { DetailRecordModule } from 'src/detail-record/detail-record.module';
import { GameModule } from '../game/game.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([GameRecordEntity]),
    UserModule,
    DetailRecordModule,
    GameModule,
  ],
  providers: [GameRecordService],
  controllers: [GameRecordController],
  exports: [GameRecordService],
})
export class GameRecordModule {}
