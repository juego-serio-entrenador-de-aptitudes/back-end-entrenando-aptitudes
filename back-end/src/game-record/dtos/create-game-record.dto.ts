import {
  IsPositive,
  IsArray,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { CreateDetailRecordDTO } from './create-detail-record.dto';
import { Type } from 'class-transformer';

export class CreateGameRecorDTO {
  @IsNumber()
  totalPoints: number;

  @IsPositive()
  game: number;

  @IsArray()
  @IsOptional()
  @ValidateNested()
  @Type(type => CreateDetailRecordDTO)
  details?: CreateDetailRecordDTO[];
}
