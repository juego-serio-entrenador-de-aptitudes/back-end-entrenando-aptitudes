import { readFileSync } from 'fs';

export async function crearDatosPrueba(servicio: any, rutaDatos: string) {
  try {
    const registrosParseados = JSON.parse(
      readFileSync(__dirname + rutaDatos, 'utf-8').toString(),
    );
    const respuesta = await servicio.createMany(registrosParseados);
    return { data: respuesta, mensaje: 'Registros creados para' + rutaDatos };
  } catch (e) {
    throw new Error(
      JSON.stringify({ mensaje: 'Error creando registros', error: e }),
    );
  }
}
