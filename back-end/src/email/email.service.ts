import { Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import Mail = require('nodemailer/lib/mailer');
// import * as postmark from 'postmark';

@Injectable()
export class EmailService {
  transport: Mail;
  client;
  constructor() {
    this.configureTransport();
  }

  private configureTransport() {
    this.transport = createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'polhibou@gmail.com',
        pass: 'juegoserio',
      },
    });
  }

  sendMail({
    to,
    subject,
    html,
  }: {
    to: string;
    subject: string;
    html: string;
  }): Promise<any> {
    return this.transport.sendMail({
      from: '"Educaplay 👻" <iseasyepntest@gmail.com>',
      to,
      subject,
      html,
    });
  }
}
