import { ROL } from '../definitions/rol.definition';

export interface UserInterface {
  firstName: string;
  lastNAme: string;
  email: string;
  password: string;
  rol: ROL;
  avatarURL: string;
}
