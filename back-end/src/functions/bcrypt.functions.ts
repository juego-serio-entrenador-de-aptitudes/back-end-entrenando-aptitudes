import bcrypt = require('bcrypt');

export const comparePlainTextWithHash = (
  plainText: string,
  hash: string,
): boolean => {
  return bcrypt.compareSync(plainText, hash);
};
