import { IsNumberString } from 'class-validator';

export class DetailsPerRecordDTO {
  @IsNumberString()
  gameRecordId: number;
}
