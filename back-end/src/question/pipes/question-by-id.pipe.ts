import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { QuestionEntity } from '../question.entity';
import { QuestionService } from '../question.service';

@Injectable()
export class QuestionByIdPipe
  implements PipeTransform<string, Promise<QuestionEntity>> {
  constructor(private readonly _questionService: QuestionService) {}

  transform(id: string, metadata: ArgumentMetadata): Promise<QuestionEntity> {
    return this._questionService.findOneOrFail(id).catch(() => {
      throw new NotFoundException(`la pregunta ${id} no existe`);
    });
  }
}
