import * as fs from 'fs';

export const generateChangePasswordMessage = (key: string) => {
  const template = fs.readFileSync(__dirname + '/../../templates/password.template.html', 'utf8');
  const message = template.replace('URL_RECUPERACION', `http://educaplay.epn.edu.ec/reset-password/${key}`);
  return message;
};
