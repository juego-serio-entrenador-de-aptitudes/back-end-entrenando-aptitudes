import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { UserEntity } from 'src/user/user.entity';
import { GameEntity } from 'src/game/game.entity';
import { DetailRecordEntity } from 'src/detail-record/detail-record.entity';

@Entity('game_record')
export class GameRecordEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'total_points',
    type: 'integer',
  })
  totalPoints: number;

  @CreateDateColumn()
  date: string;

  @ManyToOne(
    type => UserEntity,
    user => user.records,
    {
      onDelete: 'CASCADE',
    },
  )
  user: UserEntity | number;

  @ManyToOne(
    type => GameEntity,
    game => game.records,
  )
  game: GameEntity | number;

  @OneToMany(
    type => DetailRecordEntity,
    detail => detail.gameRecord,
  )
  details: DetailRecordEntity[];
}
