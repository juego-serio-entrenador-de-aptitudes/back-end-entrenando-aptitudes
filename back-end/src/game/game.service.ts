import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { GameEntity } from './game.entity';
import { Repository } from 'typeorm';
import { GameInterface } from '../interfaces/game.interface';

@Injectable()
export class GameService {
  constructor(
    @InjectRepository(GameEntity)
    private readonly _gameRepository: Repository<GameEntity>,
  ) {}

  createMany(game: GameEntity[]) {
    return this._gameRepository.save(game);
  }

  async findOneById(id: number) {
    return await this._gameRepository.findOne(id);
  }

  findOneOrFail(id: string) {
    return this._gameRepository.findOneOrFail(id);
  }
}
