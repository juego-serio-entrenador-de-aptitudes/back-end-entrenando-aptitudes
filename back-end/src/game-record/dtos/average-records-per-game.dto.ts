import { IsEnum, IsNumberString } from 'class-validator';
import { SUBJECTS } from '../../definitions/subjects.definition';

export class AverageRecordsPerGameDTO {
  @IsEnum(SUBJECTS)
  subjectName: string;

  @IsNumberString()
  userId: string;
}
