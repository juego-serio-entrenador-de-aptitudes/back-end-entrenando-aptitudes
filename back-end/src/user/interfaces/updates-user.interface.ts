import { ROL } from '../../definitions/rol.definition';

export class UpdatesUserInterface {
  firstName?: string;
  lastNAme?: string;
  email?: string;
  avatarURL?: string;
}
