import { GameRecordInterface } from './game-record.interface';

export interface DetailRecordInterface {
  questionText: string;
  questionImg: string;
  userAnswer: string;
  correctAnswer: string;
  success: boolean;
  gameRecord: GameRecordInterface;
}
