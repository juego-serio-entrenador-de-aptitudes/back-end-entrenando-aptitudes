import {
  IsString,
  IsNumberString,
  IsOptional,
  IsPositive,
  IsEnum,
  Length,
  MaxLength,
} from 'class-validator';
import { GameIdDefinitions } from '../../definitions/game-id.definitions';

export class SearchQuestionDTO {
  @IsString()
  @IsOptional()
  @MaxLength(1000)
  questionText: string;

  @IsNumberString()
  @IsOptional()
  take: string;

  @IsNumberString()
  @IsOptional()
  skip: string;

  @IsOptional()
  game: string;
}
