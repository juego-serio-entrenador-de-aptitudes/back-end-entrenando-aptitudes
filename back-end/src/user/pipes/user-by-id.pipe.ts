import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { UserEntity } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class UserByIdPipe
  implements PipeTransform<string, Promise<UserEntity>> {
  constructor(private readonly _userService: UserService) {}

  async transform(
    userId: string,
    metadata: ArgumentMetadata,
  ): Promise<UserEntity> {
    return await this._userService.findById(userId).catch(() => {
      throw new NotFoundException(`usuario ${userId} no encontrado`);
    });
  }
}
