import {
  Injectable,
  ForbiddenException,
  ConflictException,
  BadGatewayException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like, Brackets, Not } from 'typeorm';
import { UserEntity } from './user.entity';
import { ROL } from '../definitions/rol.definition';
import { UpdatesUserInterface } from './interfaces/updates-user.interface';
import { comparePlainTextWithHash } from '../functions/bcrypt.functions';
import { CHANGE_TOKEN_FACTOR } from '../definitions/change-token-factor';
import { EmailService } from '../email/email.service';
import { generateChangePasswordMessage } from '../email/email.constants';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly _userRepository: Repository<UserEntity>,
    private readonly _emailService: EmailService,
  ) {}

  createMany(newUser) {
    return this._userRepository.save(newUser);
  }

  findByEmail(email: string): Promise<UserEntity> {
    return this._userRepository.findOneOrFail({
      where: {
        email,
      },
    });
  }

  findByToken(token: string): Promise<UserEntity> {
    return this._userRepository.findOneOrFail({
      where: {
        passwordResetToken: token,
      },
    });
  }

  findById(id: string | number) {
    return this._userRepository.findOneOrFail(id);
  }

  updateUser(id: number, updates: UpdatesUserInterface) {
    return this._userRepository.update({ id }, updates);
  }

  searchUsers(
    pattern: string = '',
    skip: number = 0,
    take: number = 10,
  ): Promise<[UserEntity[], number]> {
    return this._userRepository.findAndCount({
      where: [
        { firstName: Like(`%${pattern}%`), rol: Not('ADMIN') },
        { lastName: Like(`%${pattern}%`), rol: Not('ADMIN') },
        { email: Like(`%${pattern}%`), rol: Not('ADMIN') },
      ],
      skip,
      take,
    });
  }

  countAllUsers(pattern: string = 'sinpatron@@'): Promise<number> {
    return this._userRepository.count({
      where: [
        { firstName: Like(`%${pattern}%`), rol: Not('ADMIN') },
        { lastName: Like(`%${pattern}%`), rol: Not('ADMIN') },
        { email: Like(`%${pattern}%`), rol: Not('ADMIN') },
      ],
    });
  }

  deleteUser(id: number) {
    return this._userRepository
      .findOne(id)
      .then(user => this._userRepository.remove(user));
  }

  findFollowingAndFollowRequestPlayers(
    tutorId: number,
    pattern: string = '',
    skip: number = 0,
    take: number = 5,
  ): Promise<any[]> {
    return this._userRepository
      .createQueryBuilder('player')
      .leftJoin('player.followers', 'following')
      .leftJoin('player.followerRequest', 'followRequest')
      .select('player.email', 'email')
      .addSelect('CONCAT(player.first_name, " ", player.last_name)', 'name')
      .addSelect('player.id', 'id')
      .addSelect(
        'IF(following.id = :tutorId, "Amigo", "Solicitud enviada")',
        'state',
      )
      .where(
        new Brackets(qb => {
          qb.where('following.id = :tutorId', {
            tutorId,
          }).orWhere('followRequest.id = :tutorId', { tutorId });
        }),
      )
      .andWhere(
        new Brackets(qb => {
          qb.where('player.first_name like :pattern', {
            pattern: `%${pattern}%`,
          })
            .orWhere('player.last_name like :pattern')
            .orWhere('player.email like :pattern');
        }),
      )
      .offset(skip)
      .limit(take)
      .getRawMany();
  }

  async findPlayersNotFollowed(
    tutorId: number,
    patternPlayer: string = '',
  ): Promise<UserEntity[]> {
    const playersFollowed = await this.findFollowingAndFollowRequestPlayers(
      tutorId,
      patternPlayer,
      0,
      0,
    );

    let allPlayers = await this._userRepository
      .createQueryBuilder('player')
      .select('CONCAT(player.first_name, " ", player.last_name)', 'name')
      .addSelect('player.email', 'email')
      .addSelect('"No seguido"', 'state')
      .where(new Brackets(qb => {
        return qb
          .where('player.first_name like :pattern', {
            pattern: `%${patternPlayer}%`,
          })
          .orWhere('player.last_name like :pattern')
          .orWhere('player.email like :pattern');
      }))
      .andWhere('player.rol = "JUGADOR"')
      .getRawMany();

    allPlayers = allPlayers.map(player => {
      const isFriend = playersFollowed.find(
        playerFollowed => playerFollowed.email === player.email,
      );
      if (isFriend) {
        player.state = isFriend.state;
      }
      return player;
    });

    return allPlayers;
  }

  findPlayers(
    pattern: string = 'sinpatron@@',
    skip: number = 0,
    take: number = 0,
  ): Promise<UserEntity[]> {
    return this._userRepository.find({
      where: [{ email: pattern, rol: ROL.JUGADOR }],
      skip,
      take,
    });
  }

  countTotalPlayers(pattern: string = 'sinpatron@@'): Promise<number> {
    return this._userRepository.count({
      where: [
        { firstName: Like(`%${pattern}%`), rol: ROL.JUGADOR },
        { lastName: Like(`%${pattern}%`), rol: ROL.JUGADOR },
        { email: Like(`%${pattern}%`), rol: ROL.JUGADOR },
      ],
    });
  }

  async createFollowRequest(
    tutorId: number,
    playerId: number,
  ): Promise<UserEntity> {
    const player = await this._userRepository.findOne({
      where: {
        id: playerId,
      },
    });

    const tutor = await this._userRepository.findOne({
      where: {
        id: tutorId,
      },
      relations: ['followingRequest'],
    });

    tutor.followingRequest.push(player);
    return this._userRepository.save(tutor);
  }

  async createFollowingAndDeleteRequest(
    tutorId: number,
    playerId: number,
  ): Promise<UserEntity> {
    const player = await this._userRepository.findOne({
      where: {
        id: playerId,
      },
    });

    const tutor = await this._userRepository.findOne({
      where: {
        id: tutorId,
      },
      relations: ['followingRequest', 'following', 'followers'],
    });

    tutor.following.push(player);
    tutor.followers.push(player);

    tutor.followingRequest = tutor.followingRequest.filter(
      playerRequest => playerRequest.id !== player.id,
    );
    return this._userRepository.save(tutor);
  }

  async unfollowPlayer(tutorId: number, playerId: number): Promise<UserEntity> {
    const tutor = await this._userRepository.findOne({
      where: {
        id: tutorId,
      },
      relations: ['following'],
    });
    tutor.following = tutor.following.filter(player => player.id !== playerId);
    return this._userRepository.save(tutor);
  }
  async deleteFollowRequest(
    tutorId: number,
    playerId: number,
  ): Promise<UserEntity> {
    const tutor = await this._userRepository.findOne({
      where: {
        id: tutorId,
      },
      relations: ['followingRequest'],
    });
    tutor.followingRequest = tutor.followingRequest.filter(
      player => player.id !== playerId,
    );
    return this._userRepository.save(tutor);
  }

  async updatePassword(
    user: UserEntity,
    password: string,
  ): Promise<{ message: string }> {
    const samePassword = this.verifyPassword(password, user);
    if (samePassword) {
      throw new ConflictException('el nuevo password es igual al anterior');
    }
    user.password = password;
    user.passwordResetToken = null;
    try {
      await this._userRepository.save(user);
      return { message: 'password actualizado' };
    } catch (e) {
      throw new ConflictException('no se pudo actualizar el password');
    }
  }

  async updatePasswordWithCurrentPassword(
    userId: number,
    currentPassword: string,
    newPassword: string,
  ): Promise<{ message: string }> {
    const user = await this.findById(userId);
    const isCorrectPassword = this.verifyPassword(currentPassword, user);

    if (isCorrectPassword) {
      return this.updatePassword(user, newPassword);
    } else {
      throw new ForbiddenException(`el password actual no es correcto`);
    }
  }

  private verifyPassword(password: string, user: UserEntity) {
    return comparePlainTextWithHash(password, user.password);
  }

  async requestPasswordResetAndSendMail(
    id: number,
  ): Promise<{ message: string }> {
    const user = await this._userRepository.findOne(id);
    const key = this.generateChangePasswordKey();
    await this.sendPasswordResetToken(key, user.email);
    return this.updateChangePasswordKey(user, key);
  }

  private sendPasswordResetToken(key: string, email: string) {
    return this._emailService
      .sendMail({
        to: email,
        subject: 'Educaplay - Cambio de contraseña',
        html: generateChangePasswordMessage(key),
      })
      .then(info => {
        console.log('enviado', info);
        return info;
      })
      .catch(error => {
        console.log('error enviand', error);
        throw new BadGatewayException('', error);
      });
  }

  private generateChangePasswordKey(): string {
    const key = Math.round(Math.random() * CHANGE_TOKEN_FACTOR).toString();
    if (key.length === 5) {
      return key;
    } else {
      return this.generateChangePasswordKey();
    }
  }

  private updateChangePasswordKey(
    user: UserEntity,
    changePasswordKey: string,
  ): Promise<{ message: string }> {
    user.passwordResetToken = changePasswordKey;
    return this._userRepository
      .save(user)
      .then(() => {
        return {
          message:
            'Se te ha enviado una clave a tu email, por favor revísalo para continuar',
        };
      })
      .catch(error => {
        throw new ConflictException(
          'no se pudo asignar un passwordChangeKey',
          error,
        );
      });
  }

  findPlayerRequestReceived(
    playerId: number,
    skip: number = 0,
    take: number = 10,
  ) {
    return this._userRepository
      .createQueryBuilder('tutor')
      .innerJoin('tutor.followingRequest', 'player')
      .select('tutor.email', 'email')
      .addSelect('CONCAT(tutor.first_name, " ", tutor.last_name)', 'name')
      .addSelect('tutor.id', 'id')
      .where('player.id = :playerId', { playerId })
      .offset(skip)
      .limit(take)
      .getRawMany();
  }

  async findTutorsId(playerId: number): Promise<number[]> {
    return await this._userRepository
      .findOneOrFail(playerId, {
        relations: ['followers'],
      })
      .then(player => {
        return player.followers.map(follower => follower.id);
      })
      .catch(() => {
        throw new NotFoundException('usuario no encontrado');
      });
  }
}
