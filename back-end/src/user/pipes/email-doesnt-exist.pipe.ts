import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  ConflictException,
} from '@nestjs/common';
import { UserService } from 'src/user/user.service';

@Injectable()
export class EmailDoesntExistPipe
  implements PipeTransform<{ email: string }, Promise<any>> {
  constructor(private readonly _userService: UserService) {}

  async transform(
    newUser: { email: string },
    metadata: ArgumentMetadata,
  ): Promise<any> {
    const userHasEmail = await this._userService
      .findByEmail(newUser.email)
      .then(userFind => userFind)
      .catch(() => null);
    if (userHasEmail) {
      throw new ConflictException({
        message: `correo ${newUser.email} ya está en uso`,
        statusCode: 409,
      });
    } else {
      return newUser;
    }
  }
}
