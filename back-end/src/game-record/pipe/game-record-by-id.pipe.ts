import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { GameRecordEntity } from '../game-record.entity';
import { GameRecordService } from '../game-record.service';

@Injectable()
export class GameRecordByIdPipe
  implements PipeTransform<string, Promise<GameRecordEntity>> {
  constructor(private readonly _gameRecordService: GameRecordService) {}

  transform(id: string, metadata: ArgumentMetadata): Promise<GameRecordEntity> {
    return this._gameRecordService.findOneOrFail(id).catch(() => {
      throw new NotFoundException(`el registro ${id} de juego no existe`);
    });
  }
}
