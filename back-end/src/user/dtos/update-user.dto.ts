import {
  IsNumberString,
  IsString,
  IsOptional,
  IsEmail,
  IsUrl,
  Matches,
  Length,
  IsEnum,
} from 'class-validator';
import { NAME_EXPRESSION } from '../../regular-expressions/name.expression';
import { ROL } from '../../definitions/rol.definition';

export class UpdateUserDTO {
  @IsString()
  @IsOptional()
  @Matches(NAME_EXPRESSION)
  @Length(3, 30)
  firstName?: string;

  @IsString()
  @IsOptional()
  @Matches(NAME_EXPRESSION)
  @Length(3, 30)
  lastName?: string;

  @IsEmail()
  @IsOptional()
  email?: string;

  @IsOptional()
  @IsUrl()
  avatarURL?: string;
}
