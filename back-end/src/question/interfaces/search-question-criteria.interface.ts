import { GameIdDefinitions } from '../../definitions/game-id.definitions';

export interface SearchQuestionCriteriaInterface {
  questionText?: string;
  game?: GameIdDefinitions;
  skip?: number;
  take?: number;
}
