import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
  ForbiddenException,
} from '@nestjs/common';
import { UserEntity } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class UserByTokenPipe
  implements PipeTransform<string, Promise<UserEntity>> {
  constructor(private readonly _userService: UserService) {}

  async transform(
    token: string,
    metadata: ArgumentMetadata,
  ): Promise<UserEntity> {
    return await this._userService.findByToken(token).catch(error => {
      throw new ForbiddenException(
        'La solicitud es inválida por favor solicita nuevamente un cambio de contraseña',
        error,
      );
    });
  }
}
