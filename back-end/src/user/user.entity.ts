import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';
import { ROL } from '../definitions/rol.definition';
import { GameRecordEntity } from 'src/game-record/game-record.entity';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'first_name',
    type: 'varchar',
    length: '30',
    nullable: false,
  })
  firstName: string;

  @Column({
    name: 'last_name',
    type: 'varchar',
    length: '30',
    nullable: false,
  })
  lastName: string;

  @Column({
    name: 'email',
    type: 'varchar',
    length: '40',
    nullable: false,
    unique: true,
  })
  email: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: '256',
    nullable: false,
  })
  password: string;

  @Column({
    name: 'rol',
    type: 'enum',
    default: ROL.JUGADOR,
    enum: [ROL.JUGADOR, ROL.TUTOR, ROL.ADMIN, ROL.GUEST],
    nullable: false,
  })
  rol: string;

  @Column({
    name: 'avatar_url',
    type: 'varchar',
    nullable: true,
  })
  avatarUrl: string;

  @Column({
    nullable: true,
    name: 'password_reset_token',
    type: 'varchar',
  })
  passwordResetToken: string;

  @OneToMany(
    type => GameRecordEntity,
    record => record.user,
  )
  records: GameRecordEntity[];

  @ManyToMany(
    type => UserEntity,
    user => user.followers,
    {cascade: true},
  )
  @JoinTable()
  following: UserEntity[]; // los que sigo

  @ManyToMany(
    type => UserEntity,
    user => user.following,
  )
  followers: UserEntity[]; // los que me siguen

  @ManyToMany(
    type => UserEntity,
    user => user.followerRequest,
    {cascade: true},
  )
  @JoinTable()
  followingRequest: UserEntity[]; // los que quiero seguir

  @ManyToMany(
    type => UserEntity,
    user => user.followingRequest,
  )
  followerRequest: UserEntity[]; // los que me quieren seguir
}
