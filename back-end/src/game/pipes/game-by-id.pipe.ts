import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { GameEntity } from '../game.entity';
import { GameService } from '../game.service';

@Injectable()
export class GameByIdPipe
  implements PipeTransform<string, Promise<GameEntity>> {
  constructor(private readonly _gameService: GameService) {}

  transform(id: string, metadata: ArgumentMetadata): Promise<GameEntity> {
    return this._gameService.findOneOrFail(id).catch(() => {
      throw new NotFoundException('el juego no existe');
    });
  }
}
