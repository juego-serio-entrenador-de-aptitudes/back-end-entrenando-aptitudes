import { Injectable } from '@nestjs/common';
import { GameRecordEntity } from './game-record.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, QueryBuilder } from 'typeorm';
import { DetailRecordService } from '../detail-record/detail-record.service';
import { DetailRecordEntity } from '../detail-record/detail-record.entity';
import { GameEntity } from '../game/game.entity';

@Injectable()
export class GameRecordService {
  constructor(
    @InjectRepository(GameRecordEntity)
    private readonly _gameRecordRepository: Repository<GameRecordEntity>,
    private readonly _detailRecordService: DetailRecordService,
  ) {}

  findAverageScore(gameId: number): Promise<{ average: number }> {
    return this._gameRecordRepository
      .createQueryBuilder('game_record')
      .select('AVG(game_record.total_points)', 'average')
      .where('game_record.game = :gameId', { gameId })
      .getRawOne();
  }

  findLastUserScore(userId: number, gameId: number): Promise<{ last: number }> {
    return this._gameRecordRepository
      .createQueryBuilder('game_record')
      .select('game_record.total_points', 'last')
      .where('game_record.user = :userId', { userId })
      .andWhere('game_record.game = :gameId', { gameId })
      .orderBy('game_record.date', 'DESC')
      .limit(1)
      .getRawOne();
  }

  async createMany(gameRecord): Promise<GameRecordEntity | GameRecordEntity[]> {
    const record = await this._gameRecordRepository.save(gameRecord);
    const haveDetails = gameRecord.details;
    if (haveDetails) {
      gameRecord.details = this.asignRecordIdToDetails(
        gameRecord.details,
        record.id,
      );
      await this._detailRecordService.createMany(gameRecord.details);
    }

    return gameRecord;
  }

  async createManyRecords(
    gameRecord,
    userId,
  ): Promise<GameRecordEntity | GameRecordEntity[]> {
    const recordWithUser = { ...gameRecord, user: userId };
    return this.createMany(recordWithUser);
  }

  asignRecordIdToDetails(
    details: DetailRecordEntity[],
    recordId,
  ): DetailRecordEntity[] {
    return details.map(detail => {
      detail.gameRecord = recordId;
      return detail;
    });
  }

  findOneOrFail(id: string): Promise<GameRecordEntity> {
    return this._gameRecordRepository.findOneOrFail(id);
  }

  findRecordsPerGame(
    userId: number,
    gameId: number,
    skip = 0,
    take = 0,
  ): Promise<GameRecordEntity[]> {
    return this._gameRecordRepository.find({
      where: {
        user: userId,
        game: gameId,
      },
      skip,
      take,
    });
  }

  averageRecordsPerSubject(
    userId: number,
  ): Promise<Array<{ average: number; subject: string }>> {
    return this._gameRecordRepository
      .createQueryBuilder()
      .select('AVG(record.total_points)', 'average')
      .addSelect('record.subject', 'subject')
      .where('record.user = :userId', { userId })
      .from(this.joinRecordsWithSubject, 'record')
      .groupBy('subject')
      .getRawMany();
  }

  private joinRecordsWithSubject(subQuery: QueryBuilder<GameRecordEntity>) {
    return subQuery
      .select('game.subject', 'subject')
      .addSelect('game_record.user', 'user')
      .addSelect('game_record.total_points', 'total_points')
      .addSelect('game_record.id', 'id')
      .addSelect('game.name', 'game_name')
      .from(GameEntity, 'game')
      .addFrom(GameRecordEntity, 'game_record')
      .where('game.id = game_record.game');
  }

  averageRecordsPerGame(
    userId: number,
    subject: string,
  ): Promise<Array<{ average: number; game: string }>> {
    return this._gameRecordRepository
      .createQueryBuilder()
      .select('AVG(record.total_points)', 'average')
      .addSelect('record.game_name', 'game')
      .where('record.user = :userId', { userId })
      .andWhere('record.subject = :subject', { subject })
      .from(this.joinRecordsWithSubject, 'record')
      .groupBy('game_name')
      .getRawMany();
  }

  async coinsPerUser(
    userId: number | string,
  ): Promise<Array<{ gameName: string; coins: number; gameId: number }>> {
    const maxPointsPerGame = await this._gameRecordRepository
      .createQueryBuilder('record')
      .addFrom(GameEntity, 'game')
      .select('MAX(record.total_points)', 'max_points')
      .addSelect('game.name', 'game_name')
      .addSelect('game.id', 'game_id')
      .where('record.game = game.id')
      .andWhere('record.user = :userId', { userId })
      .groupBy('game.name')
      .addGroupBy('game.id')
      .getRawMany();

    return maxPointsPerGame.map(gameMaxPoints => {
      if (gameMaxPoints.max_points > 900) {
        return {
          gameName: gameMaxPoints.game_name,
          coins: 3,
          gameId: gameMaxPoints.game_id,
        };
      }
      if (gameMaxPoints.max_points > 800) {
        return {
          gameName: gameMaxPoints.game_name,
          coins: 2,
          gameId: gameMaxPoints.game_id,
        };
      }
      if (gameMaxPoints.max_points > 700) {
        return {
          gameName: gameMaxPoints.game_name,
          coins: 1,
          gameId: gameMaxPoints.game_id,
        };
      }
      return {
        gameName: gameMaxPoints.game_name,
        coins: 0,
        gameId: gameMaxPoints.game_id,
      };
    });
  }
}
