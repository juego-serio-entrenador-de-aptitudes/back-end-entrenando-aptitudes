import { IsOptional, IsString, Length } from 'class-validator';
import { CreateQuestionDto } from '../../question/dtos/create-question.dto';

export class GameDto {
  @IsString()
  @Length(3, 40)
  subject: string;

  @IsString()
  name: string;

  @IsOptional()
  questions?: CreateQuestionDto[];

  // @IsOptional()
  // records: CreateGameRecordDto;
}
