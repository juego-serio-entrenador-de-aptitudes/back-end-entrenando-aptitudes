
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ROL } from '../definitions/rol.definition';

@Injectable()
export class IsAdminGuard implements CanActivate {
    canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
        const request = context.switchToHttp().getRequest();
        return this.isAdmin(request.user);
    }

    isAdmin(user: {rol: ROL}): boolean {
        return user.rol === ROL.ADMIN;
    }
}
