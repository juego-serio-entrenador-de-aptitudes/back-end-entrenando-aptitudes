export enum SUCCESS_MESSAGES {
  createUser = 'Usuario creado exitosamente',
}

export enum ERROR_MESSAGES {
  createUser = 'Hubo un error al crear el usuario',
}
