import {
  IsString,
  IsInt,
  IsEmail,
  IsEnum,
  Length,
  IsOptional,
  Matches,
} from 'class-validator';
import { ROL } from '../../definitions/rol.definition';
import { NAME_EXPRESSION } from '../../regular-expressions/name.expression';
import { PASSWORD_EXPRESSION } from '../../regular-expressions/password.expression';

export class CreateUserDto {
  @IsString()
  @Length(3, 30)
  @Matches(NAME_EXPRESSION)
  firstName: string;

  @IsString()
  @Length(3, 30)
  @Matches(NAME_EXPRESSION)
  lastName: number;

  @IsString()
  @Length(3, 40)
  @IsEmail()
  email: string;

  @Matches(PASSWORD_EXPRESSION)
  password: string;

  @IsString()
  @IsOptional()
  avatarUrl: string;
}
