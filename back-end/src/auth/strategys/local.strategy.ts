import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import {
  Injectable,
  UnauthorizedException,
  BadRequestException,
} from '@nestjs/common';
import { AuthService } from '../auth.service';
import { validateOrReject } from 'class-validator';
import { LoginDTO } from '../../dtos/login.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  async validate(email: string, password: string): Promise<any> {
    await this.validateRequest(email, password);
    const user = await this.authService.validateUser(email, password);
    if (!user) {
      throw new UnauthorizedException('login incorrecto');
    }
    return user;
  }

  async validateRequest(email: string, password: string) {
    const loginDTO = new LoginDTO();
    loginDTO.email = email;
    loginDTO.password = password;
    await validateOrReject(loginDTO, { whitelist: true })
      .then()
      .catch(error => {
        throw new BadRequestException(error);
      });
  }
}
