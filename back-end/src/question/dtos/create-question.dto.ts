import { IsEnum, IsOptional, IsString, Length } from 'class-validator';
import { GameDto } from '../../game/dtos/game.dto';
import { GameIdDefinitions } from '../../definitions/game-id.definitions';

export class CreateQuestionDto {
  @IsString()
  @Length(2, 1500)
  @IsOptional()
  questionText: string;

  @IsString()
  @IsOptional()
  questionImg: string;

  @IsString()
  @Length(1, 1000)
  @IsOptional()
  correctOptionText: string;

  @IsString()
  @IsOptional()
  correctOptionImg: string;

  @IsString()
  @Length(1, 1000)
  @IsOptional()
  firstOptionText: string;

  @IsString()
  @IsOptional()
  firstOptionImg: string;

  @IsString()
  @Length(1, 1000)
  @IsOptional()
  secondOptionText: string;

  @IsString()
  @IsOptional()
  secondOptionImg: string;

  @IsString()
  @Length(1, 1000)
  @IsOptional()
  thirdOptionText: string;

  @IsString()
  @IsOptional()
  thirdOptionImg: string;

  @IsString()
  @IsOptional()
  explanation: string;

  @IsEnum(GameIdDefinitions)
  game: GameDto;
}
