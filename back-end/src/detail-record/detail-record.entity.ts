import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { GameRecordEntity } from 'src/game-record/game-record.entity';

@Entity('detail-record')
export class DetailRecordEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'question_text',
    type: 'varchar',
    default: '',
    length: 1500,
    nullable: true,
  })
  questionText: string;

  @Column({
    name: 'question_img',
    type: 'varchar',
    default: '',
    nullable: true,
  })
  questionImg: string;

  @Column({
    name: 'user_answer',
    type: 'varchar',
  })
  userAnswer: string;

  @Column({
    name: 'correct_answer',
    type: 'varchar',
  })
  correctAnswer: string;

  @Column({
    name: 'success',
    type: 'boolean',
  })
  success: boolean;

  @ManyToOne(
    type => GameRecordEntity,
    gameRecord => gameRecord.details,
    {
      onDelete: 'CASCADE',
    },
  )
  gameRecord: GameRecordEntity | string;
}
