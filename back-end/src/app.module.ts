import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ENVIRONMENT_LOCAL_MYSQL, CREATE_TEST_DATA } from './environment/env.local';
import { ENVIRONMENT_MYSQL } from './environment/env';
import { UserEntity } from './user/user.entity';
import { HashPasswordTrigger } from './user/triggers/hash-password.trigger';
import { GameModule } from './game/game.module';
import { GameEntity } from './game/game.entity';
import { QuestionEntity } from './question/question.entity';
import { QuestionModule } from './question/question.module';
import { GameRecordModule } from './game-record/game-record.module';
import { GameRecordEntity } from './game-record/game-record.entity';
import { DetailRecordModule } from './detail-record/detail-record.module';
import { DetailRecordEntity } from './detail-record/detail-record.entity';
import { GameService } from './game/game.service';
import { QuestionService } from './question/question.service';
import { crearDatosPrueba } from './datos-de-prueba/crear-datos-prueba';
import { GameRecordService } from './game-record/game-record.service';
import { UserService } from './user/user.service';
import { join } from 'path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { DetailRecordService } from './detail-record/detail-record.service';
import { AuthModule } from './auth/auth.module';
import { EmailModule } from './email/email.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    UserModule,
    TypeOrmModule.forRoot(
      ENVIRONMENT_LOCAL_MYSQL
        ? {
            type: 'mysql',
            host: ENVIRONMENT_LOCAL_MYSQL.host,
            port: ENVIRONMENT_LOCAL_MYSQL.port,
            username: ENVIRONMENT_LOCAL_MYSQL.username,
            password: ENVIRONMENT_LOCAL_MYSQL.password,
            database: ENVIRONMENT_LOCAL_MYSQL.database,
            entities: [
              UserEntity,
              GameEntity,
              QuestionEntity,
              GameRecordEntity,
              DetailRecordEntity,
            ],
            dropSchema: ENVIRONMENT_LOCAL_MYSQL.dropSchema,
            synchronize: true,
            subscribers: [HashPasswordTrigger],
          }
        : {
            type: 'postgres',
            host: ENVIRONMENT_MYSQL.host,
            port: ENVIRONMENT_MYSQL.port,
            username: ENVIRONMENT_MYSQL.username,
            password: ENVIRONMENT_MYSQL.password,
            database: ENVIRONMENT_MYSQL.database,
            ssl: true,
            entities: [
              UserEntity,
              GameEntity,
              QuestionEntity,
              GameRecordEntity,
              DetailRecordEntity,
            ],
            dropSchema: true,
            synchronize: true,
            subscribers: [HashPasswordTrigger],
          },
    ),
    GameModule,
    QuestionModule,
    GameRecordModule,
    DetailRecordModule,
    AuthModule,
    EmailModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(
    private readonly _gameService: GameService,
    private readonly _questionService: QuestionService,
    private readonly _gameRecordService: GameRecordService,
    private readonly _userService: UserService,
    private readonly _detailRecordService: DetailRecordService,
  ) {
    if (CREATE_TEST_DATA) {
      this.crearDatosDePrueba()
      .then(datos => {
        // tslint:disable-next-line: no-console
        // console.log('exito:', datos);
      })
      .catch(error => {
        // tslint:disable-next-line: no-console
        console.log('error:', error);
      });
    }
  }

  async crearDatosDePrueba() {
    try {
      const gameData = await crearDatosPrueba(this._gameService, '/games.json');
      const questionsTriviaFeria = await crearDatosPrueba(
        this._questionService,
        '/questions-trivia-feria.json',
      );
      const questionsAgilidadNumerica = await this._questionService.createAgilidadNumericaQuestions();
      const questionsAnalogiasMusicales = await crearDatosPrueba(
        this._questionService,
        '/questions-analogias-musicales.json',
      );
      const questionsReglaTres = await crearDatosPrueba(
        this._questionService,
        '/questions-regla-tres.json',
      );
      const questionsTriviaMillonaria = await crearDatosPrueba(
        this._questionService,
        '/questions-trivia-millonaria.json',
      );
      const questionsAutopista = await crearDatosPrueba(
        this._questionService,
        '/questions-autopista.json',
      );
      const questionsElClasificador = await crearDatosPrueba(
        this._questionService,
        '/questions-el-clasificador.json',
      );

      const questionsSaltandoEnCiencias = await crearDatosPrueba(
        this._questionService,
        '/questions-saltando-en-ciencias.json',
      );

      const questionsVerdadOFalsedad = await crearDatosPrueba(
        this._questionService,
        '/questions-verdad-falsedad.json',
      );

      const questionsTriviaIsla = await crearDatosPrueba(
        this._questionService,
        '/questions-trivia-isla.json',
      );

      const questionsTourEcuador = await crearDatosPrueba(
        this._questionService,
        '/questions-tour-ecuador.json',
      );

      const questionsCircuitoElectrico = await crearDatosPrueba(
        this._questionService,
        '/questions-circuito-electrico.json',
      );

      const questionsAhogadoEnLava = await crearDatosPrueba(
        this._questionService,
        '/questions-ahogado-en-lava.json',
      );

      const userData = await crearDatosPrueba(this._userService, '/users.json');

      const gameRecordData = await crearDatosPrueba(
        this._gameRecordService,
        '/game-records.json',
      );
      const gameRecordDetails = await crearDatosPrueba(
        this._detailRecordService,
        '/detail-records.json',
      );
      await this._userService.createFollowRequest(2, 4);
      await this._userService.createFollowRequest(2, 5);
      await this._userService.createFollowRequest(2, 6);
      await this._userService.createFollowRequest(2, 3);
      // await this._userService.createFollowingAndDeleteRequest(2, 3);
      // await this._userService.createFollowingAndDeleteRequest(2, 4);
      // await this._userService.createFollowingAndDeleteRequest(2, 5);
      // await this._userService.createFollowingAndDeleteRequest(2, 6);

      // tslint:disable-next-line: no-console
      console.log('MensajeGame:', gameData.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsTriviaFeria.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsAnalogiasMusicales.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsTriviaMillonaria.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsAutopista.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsElClasificador.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsSaltandoEnCiencias.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensage:', questionsVerdadOFalsedad.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensage:', questionsCircuitoElectrico.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensage:', questionsAhogadoEnLava.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', gameRecordData.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', userData.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsTriviaIsla.mensaje);
      // tslint:disable-next-line: no-console
      console.log('Mensaje:', questionsTourEcuador.mensaje);
    } catch (e) {
      // tslint:disable-next-line: no-console
      console.error('Error con datos de prueba', e);
      return e;
    }
  }
}
