import { Entity, Column, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { GameEntity } from '../game/game.entity';

@Entity('question')
export class QuestionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'question_text',
    type: 'varchar',
    length: 1500,
    nullable: true,
  })
  questionText: string;

  @Column({
    name: 'question_img',
    type: 'varchar',
    nullable: true,
  })
  questionImg: string;

  @Column({
    name: 'correct_option_text',
    type: 'varchar',
    length: 1000,
    nullable: true,
  })
  correctOptionText: string;

  @Column({
    name: 'correct_option_img',
    type: 'varchar',
    nullable: true,
  })
  correctOptionImg: string;

  @Column({
    name: 'first_option_text',
    type: 'varchar',
    length: 1000,
    nullable: true,
  })
  firstOptionText: string;

  @Column({
    name: 'first_option_img',
    type: 'varchar',
    nullable: true,
  })
  firstOptionImg: string;

  @Column({
    name: 'second_option_text',
    type: 'varchar',
    length: 1000,
    nullable: true,
  })
  secondOptionText: string;

  @Column({
    name: 'second_option_img',
    type: 'varchar',
    nullable: true,
  })
  secondOptionImg: string;

  @Column({
    name: 'third_option_text',
    type: 'varchar',
    length: 1000,
    nullable: true,
  })
  thirdOptionText: string;

  @Column({
    name: 'third_option_img',
    type: 'varchar',
    nullable: true,
  })
  thirdOptionImg: string;

  @Column({
    name: 'explanation',
    type: 'varchar',
    nullable: true,
  })
  explanation: string;

  @ManyToOne(
    type => GameEntity,
    game => game.questions,
  )
  game: GameEntity;
}
