import { IsNumberString } from 'class-validator';

export class AverageRecordDto {
  @IsNumberString()
  gameId: string;
}
