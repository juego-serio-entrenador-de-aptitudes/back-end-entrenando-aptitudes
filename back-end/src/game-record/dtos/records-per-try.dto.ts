import { IsNumberString } from 'class-validator';

export class RecordsPerTryDTO {
  @IsNumberString()
  gameId: number;

  @IsNumberString()
  userId: number;
}
