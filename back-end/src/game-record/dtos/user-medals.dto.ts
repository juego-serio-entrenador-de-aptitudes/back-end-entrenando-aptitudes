import { IsNumberString } from 'class-validator';

export class UserMedalsDto {
  @IsNumberString()
  userId: string;
}
