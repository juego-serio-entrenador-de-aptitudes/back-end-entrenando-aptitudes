import { Injectable, NotFoundException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { JwtService } from '@nestjs/jwt';
import { comparePlainTextWithHash } from '../functions/bcrypt.functions';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService
      .findByEmail(username)
      .then(foundUser => {
        return foundUser;
      })
      .catch(() => {
        throw new NotFoundException(`usuario ${username} no encontrado`);
      });

    const isPasswordCorrect = comparePlainTextWithHash(pass, user.password);
    if (user && isPasswordCorrect) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { email: user.email, id: user.id, rol: user.rol }; // TODO ojo
    return {
      access_token: this.jwtService.sign(payload),
      user,
    };
  }
}
