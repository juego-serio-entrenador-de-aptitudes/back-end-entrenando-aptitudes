export enum ROL {
  JUGADOR = 'JUGADOR',
  TUTOR = 'TUTOR',
  ADMIN = 'ADMIN',
  GUEST = 'GUEST',
}
