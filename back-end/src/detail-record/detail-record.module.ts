import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DetailRecordEntity } from './detail-record.entity';
import { DetailRecordService } from './detail-record.service';

@Module({
  imports: [TypeOrmModule.forFeature([DetailRecordEntity])],
  providers: [DetailRecordService],
  exports: [DetailRecordService],
})
export class DetailRecordModule {}
