export enum SUBJECTS {
  MATH = 'matematicas',
  ABSTRACT = 'abstracto',
  SCIENCIES = 'ciencias',
  SOCIAL = 'sociales',
  LITERATURE = 'lenguaje',
}
