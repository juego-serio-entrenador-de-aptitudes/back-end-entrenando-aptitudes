import {
  EventSubscriber,
  EntitySubscriberInterface,
  InsertEvent,
  UpdateEvent,
} from 'typeorm';
import { UserEntity } from '../user.entity';
import bcrypt = require('bcrypt');
import { SALT_ROUNDS } from '../../definitions/salt-rounds';
import { comparePlainTextWithHash } from '../../functions/bcrypt.functions';

@EventSubscriber()
export class HashPasswordTrigger
  implements EntitySubscriberInterface<UserEntity> {
  listenTo() {
    return UserEntity;
  }

  beforeInsert(event: InsertEvent<UserEntity>) {
    this.hashPassword(event.entity);
  }

  beforeUpdate(event: UpdateEvent<UserEntity>) {
    const updatePassword = event.entity.password;
    if (updatePassword) {
      this.hashPassword(event.entity);
    }
  }

  private hashPassword(entity: UserEntity) {
    const salt = bcrypt.genSaltSync(SALT_ROUNDS);
    const password = entity.password;
    entity.password = bcrypt.hashSync(password, salt);
  }
}
