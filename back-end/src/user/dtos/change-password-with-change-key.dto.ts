import {
  IsString,
  Matches,
  IsPositive,
  IsNumber,
  Length,
  IsNotEmpty,
} from 'class-validator';

import { PASSWORD_EXPRESSION } from '../../regular-expressions/password.expression';

export class UpdatePasswordWithTokenDTO {
  @IsString()
  @Matches(PASSWORD_EXPRESSION)
  password: string;

  @IsString()
  @Length(5)
  @IsNotEmpty()
  token: string;
}
