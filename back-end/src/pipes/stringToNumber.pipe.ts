import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class StringToNumberPipe implements PipeTransform<any> {
  transform(value: string) {
    if (typeof value === 'string') {
      return parseInt(value, 10);
    }
    return value;
  }
}
