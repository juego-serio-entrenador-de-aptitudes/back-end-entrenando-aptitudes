import { IsEnum, IsInt, IsPositive } from 'class-validator';
import { GameIdDefinitions } from '../../definitions/game-id.definitions';

export class RandomQuestionRequestDto {
  @IsEnum(GameIdDefinitions)
  gameId: number;

  @IsInt()
  @IsPositive()
  quantity: number;
}
