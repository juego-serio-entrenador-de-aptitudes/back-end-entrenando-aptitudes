import { IsNumberString } from 'class-validator';

export class AverageRecordsPerSubjectTO {
  @IsNumberString()
  userId: string;
}
