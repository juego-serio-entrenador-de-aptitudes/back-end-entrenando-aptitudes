import { IsString, Length, IsEmail } from 'class-validator';

export class CreateFollowRequestDTO {

    @IsString()
    @Length(3, 40)
    @IsEmail()
    playerEmail: string;
}
