import {
  IsString,
  IsOptional,
  Length,
  IsNumberString,
  MaxLength,
} from 'class-validator';

export class SearchUsersDTO {
  @IsString()
  @IsOptional()
  @MaxLength(100)
  pattern: string;

  @IsOptional()
  @IsNumberString()
  skip: string;

  @IsOptional()
  @IsNumberString()
  take: string;
}
