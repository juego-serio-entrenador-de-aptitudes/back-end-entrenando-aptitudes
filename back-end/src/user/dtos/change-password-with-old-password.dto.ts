import {
  IsString,
  Length,
  IsPositive,
  IsNumber,
  Matches,
} from 'class-validator';
import { PASSWORD_EXPRESSION } from '../../regular-expressions/password.expression';

export class ChangePasswordWithOldPasswordDTO {
  @IsString()
  @Matches(PASSWORD_EXPRESSION)
  newPassword: string;

  @IsString()
  @Length(3, 30)
  currentPassword: string;

  @IsPositive()
  @IsNumber()
  userId: number;
}
