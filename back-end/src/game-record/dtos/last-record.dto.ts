import { IsNumber, IsNumberString } from 'class-validator';

export class LastRecordDto {
  @IsNumberString()
  gameId: string;
}
