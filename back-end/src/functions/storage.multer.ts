import * as multer from 'multer';

export const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + '/../../public/images');
    },
    filename: (req, file, cb) => {

        const uniqueSuffix = getCurrentDate() + '&' + Math.round(Math.random() * 1E9) + '&';
        cb(null, uniqueSuffix + file.originalname);
    },
});

function getCurrentDate(): string {
    const todayDate = new Date();
    const dd = String(todayDate.getDate()).padStart(2, '0');
    const mm = String(todayDate.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = todayDate.getFullYear();
    return  mm + '-' + dd + '-' + yyyy;
}
