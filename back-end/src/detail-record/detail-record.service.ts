import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DetailRecordEntity } from './detail-record.entity';
import { Repository } from 'typeorm';

@Injectable()
export class DetailRecordService {
  constructor(
    @InjectRepository(DetailRecordEntity)
    private readonly _detailRecordRepository: Repository<DetailRecordEntity>,
  ) {}

  createMany(
    detailRecord: DetailRecordEntity,
  ): Promise<DetailRecordEntity | DetailRecordEntity[]> {
    return this._detailRecordRepository.save(detailRecord);
  }

  find(id: string): Promise<[DetailRecordEntity[], number]> {
    return this._detailRecordRepository.findAndCount({ gameRecord: id });
  }
}
