import { GameInterface } from './game.interface';

export interface QuestionInterface {
  id?: number;
  questionText?: string;
  questionImg?: string;
  correctOptionText?: string;
  correctOptionImg?: string;
  firstOptionText?: string;
  firstOptionImg?: string;
  secondOptionText?: string;
  secondOptionImg?: string;
  thirdOptionText?: string;
  thirdOptionImg?: string;
  explanation?: string;
  game: GameInterface;
}
