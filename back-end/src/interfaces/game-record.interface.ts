import { UserInterface } from './user.interface';
import { GameInterface } from './game.interface';
import { DetailRecordInterface } from './detail-record.interface';

export interface GameRecordInterface {
  totalPoints: number;
  date: string;
  user: UserInterface;
  game: GameInterface;
  details: DetailRecordInterface[];
}
