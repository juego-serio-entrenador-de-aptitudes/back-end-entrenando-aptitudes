import {
  Body,
  Controller,
  Get,
  Post,
  UseInterceptors,
  UploadedFile,
  Put,
  Query,
  Param,
  Delete,
  InternalServerErrorException,
  NotFoundException,
  UseGuards,
} from '@nestjs/common';
import { CreateQuestionDto } from './dtos/create-question.dto';
import { QuestionService } from './question.service';
import { RandomQuestionRequestDto } from './dtos/random-question-request.dto';
import { GameByIdPipe } from '../game/pipes/game-by-id.pipe';
import { GameEntity } from '../game/game.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateQuestionDto } from './dtos/update-question.dto';
import { QuestionByIdPipe } from './pipes/question-by-id.pipe';
import { SearchQuestionCriteriaInterface } from './interfaces/search-question-criteria.interface';
import { SearchQuestionDTO } from './dtos/search-question.dto';
import { StringToNumberPipe } from '../pipes/stringToNumber.pipe';
import { DeleteQuestionDTO } from './dtos/delete-question.dto';
import { QuestionEntity } from './question.entity';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import * as fs from 'fs';
import { DeleteImageDto } from './dtos/delete-image.dto';
import { storage } from '../functions/storage.multer';
import { IsAdminGuard } from '../guards/IsAdmin.guard';

@Controller('question')
@UseGuards(JwtAuthGuard)
export class QuestionController {
  constructor(private readonly _questionService: QuestionService) {}

  @Post()
  @UseGuards(IsAdminGuard)
  createQuestion(
    @Body() createQuestionDto: CreateQuestionDto,
    @Body('game', GameByIdPipe) game: GameEntity,
  ) {
    return this._questionService.createOne(createQuestionDto as QuestionEntity);
  }

  @Put()
  @UseGuards(IsAdminGuard)
  updateQuestion(
    @Body() updateQuestionDto: UpdateQuestionDto,
    @Body('id', QuestionByIdPipe) game: GameEntity,
  ) {
    return this._questionService.updateQuestion(updateQuestionDto);
  }

  @Post('random')
  sendRandomQuestions(
    @Body() randomQuestionRequest: RandomQuestionRequestDto,
    @Body('gameId', GameByIdPipe) game: GameEntity,
  ) {
    return this._questionService.sendRandomQuestions(
      randomQuestionRequest.gameId,
      randomQuestionRequest.quantity,
    );
  }

  @Post('upload')
  @UseGuards(IsAdminGuard)
  @UseInterceptors(
    FileInterceptor('image', {
      storage,
    }),
  )
  uploadFile(@UploadedFile() file) {
    return file;
  }

  @Delete('deleteImage')
  @UseGuards(IsAdminGuard)
  deleteImage(
    @Query() imageDataDto: DeleteImageDto,
    @Query('filename') filename: string,
  ) {
    try {
      const path = __dirname + '/../../public/images/' + filename;
      fs.unlinkSync(path);
      return { res: 'La imagen fue eliminada!' };
    } catch (error) {
      if (error.code === 'ENOENT') {
        throw new NotFoundException('La imagen no fue encontrada');
      } else {
        throw new InternalServerErrorException(error);
      }
    }
  }

  @Get('')
  @UseGuards(IsAdminGuard)
  searchQuestions(
    @Query() searchQuestion: SearchQuestionDTO,
    @Query('questionText') questionText: string,
    @Query('game', StringToNumberPipe) game: number,
    @Query('take', StringToNumberPipe) take: number,
    @Query('skip', StringToNumberPipe) skip: number,
  ): Promise<QuestionEntity[]> {
    const searchCriteria: SearchQuestionCriteriaInterface = {
      questionText,
      game,
      take,
      skip,
    };
    return this._questionService.searchQuestions(searchCriteria);
  }

  @Get('countTotal')
  countTotalQuestions(
    @Query() searchQuestion: SearchQuestionDTO,
    @Query('questionText') questionText: string,
    @Query('game', StringToNumberPipe) game: number,
  ): Promise<number> {
    const searchCriteria: SearchQuestionCriteriaInterface = {
      questionText,
      game,
    };
    return this._questionService.countTotalQuestions(searchCriteria);
  }

  @Delete(':id')
  @UseGuards(IsAdminGuard)
  async deleteQuestion(
    @Param() deleteQuestionDTO: DeleteQuestionDTO,
    @Param('id', QuestionByIdPipe) question: QuestionEntity,
  ) {
    return this._questionService
      .deleteQuestion(deleteQuestionDTO.id)
      .then(() => {
        return {
          question,
          result: 'pregunta eliminada exitosamente',
        };
      })
      .catch(error => {
        throw new InternalServerErrorException('no se pudo eliminar', error);
      });
  }
}
