import { Entity, Column, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { QuestionEntity } from '../question/question.entity';
import { GameRecordEntity } from 'src/game-record/game-record.entity';
import { SUBJECTS } from '../definitions/subjects.definition';

@Entity('game')
export class GameEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'subject',
    type: 'enum',
    enum: SUBJECTS,
  })
  subject: SUBJECTS;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 40,
  })
  name: string;

  @OneToMany(
    type => QuestionEntity,
    question => question.game,
  )
  questions: QuestionEntity[];

  @OneToMany(
    type => GameRecordEntity,
    record => record.game,
  )
  records: GameRecordEntity[];
}
