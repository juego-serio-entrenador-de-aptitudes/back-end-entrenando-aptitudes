import { IsOptional, IsString, IsBoolean, Length } from 'class-validator';

export class CreateDetailRecordDTO {
  @IsOptional()
  @IsString()
  @Length(1, 1500)
  questionText: string;

  @IsOptional()
  @IsString()
  questionImg: string;

  @IsString()
  @Length(1, 1000)
  userAnswer: string;

  @IsOptional()
  @IsString()
  @Length(1, 1000)
  correctAnswer: string;

  @IsBoolean()
  success: boolean;
}
