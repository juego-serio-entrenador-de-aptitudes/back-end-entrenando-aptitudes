import { Controller, Post, Body, Get, Param, UsePipes } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { UpdatePasswordWithTokenDTO } from './user/dtos/change-password-with-change-key.dto';
import { UserByTokenPipe } from './user/pipes/user-by-token.pipe';
import { UserEntity } from './user/user.entity';
import { RequestPasswordResetDTO } from './user/dtos/requestPasswordResetDTO';
import { UserByEmailPipe } from './user/pipes/user-by-email.pipe';
import { CreateUserDto } from './user/dtos/create-user.dto';
import { UserService } from './user/user.service';
import { EmailDoesntExistPipe } from './user/pipes/email-doesnt-exist.pipe';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService,
    private readonly _userService: UserService,
  ) {}

  @Post('updatePasswordWithToken')
  updatePasswordWithToken(
    @Body() { password }: UpdatePasswordWithTokenDTO,
    @Body('token', UserByTokenPipe) user: UserEntity,
  ) {
    return this._userService.updatePassword(user, password);
  }

  @Get('verifyToken/:token')
  verifyPassword(@Param('token', UserByTokenPipe) user: UserEntity) {
    return { message: 'El token es valido' };
  }

  @Post('requestPasswordReset')
  requestPasswordChange(
    @Body() requestPasswordResetDTO: RequestPasswordResetDTO,
    @Body('email', UserByEmailPipe) user: UserEntity,
  ) {
    return this._userService.requestPasswordResetAndSendMail(user.id);
  }

  @Post()
  @UsePipes(EmailDoesntExistPipe)
  createUser(@Body() createUserDto: CreateUserDto) {
    return this._userService.createMany(createUserDto);
  }
}
