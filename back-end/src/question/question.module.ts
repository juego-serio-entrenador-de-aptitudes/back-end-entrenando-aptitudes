import { Module } from '@nestjs/common';
import { QuestionEntity } from './question.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { QuestionController } from './question.controller';
import { QuestionService } from './question.service';
import { GameModule } from '../game/game.module';

@Module({
  imports: [TypeOrmModule.forFeature([QuestionEntity]), GameModule],
  controllers: [QuestionController],
  providers: [QuestionService],
  exports: [QuestionService],
})
export class QuestionModule {}
