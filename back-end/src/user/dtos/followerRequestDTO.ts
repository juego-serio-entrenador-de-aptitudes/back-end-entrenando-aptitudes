import { IsNumber, IsPositive } from 'class-validator';

export class FollowerRequestDTO {
  @IsNumber()
  @IsPositive()
  tutorId: number;
}
