import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  Query,
  InternalServerErrorException,
  Put,
  Req,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserEntity } from './user.entity';
import { UserByIdPipe } from './pipes/user-by-id.pipe';
import { FollowRequestDTO } from './dtos/follow-request.dto';
import { FindUserDTO } from './dtos/find-user.dto';
import { StringToNumberPipe } from '../pipes/stringToNumber.pipe';
import { SearchUsersDTO } from './dtos/search-users.dto';
import { UpdateUserDTO } from './dtos/update-user.dto';
import { ChangePasswordWithOldPasswordDTO } from './dtos/change-password-with-old-password.dto';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { FollowerRequestDTO } from './dtos/followerRequestDTO';
import { IsAdminGuard } from '../guards/IsAdmin.guard';
import { CreateFollowRequestDTO } from './dtos/createFollowRequest.dto';
import { UserByEmailPipe } from './pipes/user-by-email.pipe';

@UseGuards(JwtAuthGuard)
@Controller('user')
export class UserController {
  constructor(private readonly _userService: UserService) {}

  @Get(':id')
  findUser(
    @Param() findUserDTO: FindUserDTO,
    @Param('id', UserByIdPipe) user: UserEntity,
  ) {
    const { password, ...info } = user;
    return info;
  }

  @Put(':id')
  updateUser(
    @Body() updateUserDTO: UpdateUserDTO,
    @Param() findUserDTO: FindUserDTO,
    @Param('id', UserByIdPipe) user: UserEntity,
  ) {
    return this._userService
      .updateUser(user.id, updateUserDTO)
      .then(() => this._userService.findById(user.id))
      .catch(error => {
        throw new InternalServerErrorException('no se pudo actualizar', error);
      });
  }

  @Delete(':id')
  @UseGuards(IsAdminGuard)
  removeUser(
    @Param() findUserDTO: FindUserDTO,
    @Param('id', UserByIdPipe) user: UserEntity,
  ) {
    return this._userService.deleteUser(user.id).then(() => {
      return {
        user,
        result: 'usuario eliminado',
      };
    });
  }

  @Get('')
  @UseGuards(IsAdminGuard)
  findUsers(
    @Query() searchUsersDTO: SearchUsersDTO,
    @Query('take', StringToNumberPipe) take: number,
    @Query('skip', StringToNumberPipe) skip: number,
  ) {
    return this._userService.searchUsers(searchUsersDTO.pattern, skip, take);
  }

  @Get('count/all')
  @UseGuards(IsAdminGuard)
  countAllUsers(
      @Query() searchUsersDTO: SearchUsersDTO,
  ) {
    return this._userService.countAllUsers(searchUsersDTO.pattern);
  }

  @Get('all/players')
  findPlayers(
    @Query() searchUsersDTO: SearchUsersDTO,
    @Query('take', StringToNumberPipe) take: number,
    @Query('skip', StringToNumberPipe) skip: number,
  ) {
    return this._userService.findPlayers(searchUsersDTO.pattern, skip, take);
  }

  @Get('count/players')
  countTotalPlayers(
      @Query() searchUsersDTO: SearchUsersDTO,
  ) {
    return this._userService.countTotalPlayers(searchUsersDTO.pattern);
  }

  @Get('tutor/allFollows')
  findFollowingAndFollowingRequestPlayers(
      @Req() request,
      @Query() searchQuestion: SearchUsersDTO,
      @Query('take', StringToNumberPipe) take: number,
      @Query('skip', StringToNumberPipe) skip: number,
  ) {
    const userId = request.user.id;
    return this._userService.findFollowingAndFollowRequestPlayers(
        userId,
        searchQuestion.pattern,
        skip,
        take,
    );
  }

  @Get('player/request')
  findPlayerRequestReceived(
    @Req() request,
    @Query() searchQuestion: SearchUsersDTO,
    @Query('take', StringToNumberPipe) take: number,
    @Query('skip', StringToNumberPipe) skip: number,
  ) {
    const userId = request.user.id;
    return this._userService.findPlayerRequestReceived(userId, skip, take);
  }

  @Get('tutor/notFollowed')
  findPlayersNotFollowed(
      @Req() request,
      @Query() searchQuestion: SearchUsersDTO,
  ) {
    const userId = request.user.id;
    return this._userService.findPlayersNotFollowed(userId, searchQuestion.pattern);
  }

  @Post('createFollowRequest')
  createFollowRequest(
    @Body() createFollowDTO: CreateFollowRequestDTO,
    @Req() req,
    @Body('playerEmail', UserByEmailPipe) player: UserEntity,
  ) {
    return this._userService.createFollowRequest(req.user.id, player.id);
  }

  @Post('acceptFollowRequest')
  createFollowingAndDeleteRequest(
    @Body() followerRequestDTO: FollowerRequestDTO,
    @Body('tutorId', UserByIdPipe) tutor: UserEntity,
    @Req() req,
  ) {
    return this._userService.createFollowingAndDeleteRequest(
      tutor.id,
      req.user.id,
    );
  }

  @Post('unfollowPlayer')
  unfollowPLayer(
    @Body() followRequestDTO: FollowRequestDTO,
    @Req() req,
    @Body('playerId', UserByIdPipe) player: UserEntity,
  ) {
    return this._userService.unfollowPlayer(req.user.id, player.id);
  }

  @Post('tutor/deleteRequest')
  cancelTutorFollowRequest(
    @Body() followRequestDTO: FollowRequestDTO,
    @Req() req,
    @Body('playerId', UserByIdPipe) player: UserEntity,
  ) {
    return this._userService.deleteFollowRequest(req.user.id, player.id);
  }

  @Post('player/deleteRequest')
  cancelPLayerFollowRequest(
    @Body() followerRequestDTO: FollowerRequestDTO,
    @Req() req,
    @Body('tutorId', UserByIdPipe) tutor: UserEntity,
  ) {
    return this._userService.deleteFollowRequest(tutor.id, req.user.id);
  }

  @Post('updatePasswordWithCurrentPassword')
  updatePasswordWithCurrentPassword(
    @Body()
    { newPassword, currentPassword, userId }: ChangePasswordWithOldPasswordDTO,
    @Body('userID', UserByIdPipe) user: UserEntity,
  ) {
    return this._userService.updatePasswordWithCurrentPassword(
      userId,
      currentPassword,
      newPassword,
    );
  }
}
