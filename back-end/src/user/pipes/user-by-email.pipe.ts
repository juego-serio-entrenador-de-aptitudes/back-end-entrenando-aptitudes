import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  NotFoundException,
} from '@nestjs/common';
import { UserEntity } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';

@Injectable()
export class UserByEmailPipe
  implements PipeTransform<string, Promise<UserEntity>> {
  constructor(private readonly _userService: UserService) {}

  async transform(
    email: string,
    metadata: ArgumentMetadata,
  ): Promise<UserEntity> {
    return await this._userService.findByEmail(email).catch(() => {
      throw new NotFoundException(`usuario ${email} no encontrado`);
    });
  }
}
